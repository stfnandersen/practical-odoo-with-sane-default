# Practical Odoo Base Setup

This module will install basic modules, to make a functional  Odoo installation for most users.

## Modules installed

| Module | Summary |
| ------ | ------- |
| [account_tax_balance](https://github.com/OCA/account-financial-reporting/blob/15.0/account_tax_balance) | Compute tax balances based on date range |
| [account_usability](https://github.com/ForgeFlow/account-financial-tools/tree/15.0-mig-account_menu/account_usability) | Enabling accounting menus and views |
| [mis_builder](https://github.com/OCA/mis-builder) | Build 'Management Information System' Reports and Dashboards |
| [mis_builder_cash_flow](https://github.com/OCA/account-financial-reporting/blob/15.0/mis_builder_cash_flow) | MIS Builder Cash Flow |
| [mail_debrand](https://github.com/OCA/social/tree/15.0/mail_debrand) | Remove Odoo branding in sent emails |
| [web_no_bubble](https://github.com/OCA/web/tree/15.0/web_no_bubble) | This module removes from the web interface the bubbles |
| [web_responsive](https://github.com/OCA/web/tree/15.0/web_responsive) | This module adds responsiveness to web backend |
| [website_odoo_debranding](https://github.com/OCA/website/blob/15.0/website_odoo_debranding) | Remove Odoo Branding from Website |

## Extra dependencies
| Module | Depended by | Summary |
| ------ | ----------- | ------- |
| [date_range](https://github.com/OCA/server-ux/blob/15.0/date_range) | `account_tax_balance` | Manage all kind of date range |
| [report_xlsx](https://github.com/OCA/reporting-engine/tree/15.0/report_xlsx) | `mis_builder` | Base module to create xlsx report |


## Credits
### Contributors
* Stefan Andersen, Puma IT <stefan@stefanandersen.dk>
