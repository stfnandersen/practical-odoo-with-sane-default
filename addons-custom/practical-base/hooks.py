# Copyright 2019 Eficent Business and IT Consulting Services S.L.
#   (http://www.eficent.com)
# License LGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, SUPERUSER_ID

def install_danish_lang(env):
    lang_wiz = env['base.language.install'].create({'lang': 'da_DK'})
    lang_wiz.lang_install()

def enable_dkk_currency(env):
    env['res.currency'].with_context({'active_test': False}).search(
        [('name', '=', 'DKK')]).write({'active': True})


def post_init(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    enable_dkk_currency(env)
    install_danish_lang(env)
