# Copyright 2022 Stefan Andersen, Puma IT
#   (https://www.pumait.dk)
# License LGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Practical Odoo Base Setup",
    "version": "15.0.1.0.0",
    "author": "Stefan Andersen, Puma IT",
    "website": "https://www.pumait.dk",
    "license": "LGPL-3",
    "category": "",
    "depends": [
        'account_tax_balance',
        'account_financial_report',
        'account_invoice_constraint_chronology',
        'account_usability',
        'mail_debrand',
        'mis_builder',
        'mis_builder_cash_flow',
        'sale_automatic_workflow',
        'web_no_bubble',
        'web_responsive',
        'website_odoo_debranding',
    ],
    "data": [
        "data/res_lang_data.xml",
    ],
    "auto_install": False,
    "installable": True,
    "post_init_hook": "post_init",
}
