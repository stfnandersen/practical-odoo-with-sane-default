## General

- [ ] Is this change useful to me, or something that I think will benefit others greatly?
- [ ] Check for overlap with other MRs.
- [ ] Think carefully about the long-term implications of the change. How will it affect existing projects that are dependent on this? How will it affect my projects? If this is complicated, do I really want to maintain it forever? Is there any way it could be implemented as a separate package, for better modularity and flexibility?

## Check the Code

- [ ] If it does too much, ask for it to be broken up into smaller PRs.
- [ ] Is it consistent?

## Check the Docs

- [ ] If any new features are added, are they in `README.md`?

## Credit the Authors

- [ ] Add name and URL to `AUTHORS.md`.
- [ ] Thank them for their hard work.

## Close Issues

- [ ] Merge the MR branch. This will close the MR's issue.
- [ ] Close any duplicate or related issues that can now be closed. Write thoughtful comments explaining how the issues were resolved.


[Inspired from @audreyr](https://gist.github.com/audreyr/4feef90445b9680475f2#file-pull-request-review-checklist-md)

